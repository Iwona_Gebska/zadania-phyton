
l1=[1,2,2,2,3,3,4,5,6]
l2=[5,6,7,8]
l1_only=[number for number in l1 if number not in l2]

l1_only

from collections import Counter

counter = Counter(l1_only)

counter

def number_counts(number):
    return counter[number]

l1_only_unique = list(set(l1_only))

l1_only_unique

sorted(l1_only_unique, key=number_counts)

sorted(l1_only_unique, key=lambda number:counter[number])


