#!/usr/bin/env python
# coding: utf-8

# In[91]:


global_dict = {}
def add_to_global_dict(first_name, height):
    global_dict[first_name]= height 


# In[97]:


add_to_global_dict('Kamil',180)


# In[98]:


add_to_global_dict('Jan',180)


# In[99]:


add_to_global_dict('Tomasz',180)


# In[100]:


add_to_global_dict('Maja',170)


# In[101]:


add_to_global_dict('Kaja',170)


# In[102]:


global_dict


# In[ ]:




